#define _POSIX_SOURCE

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "input.h"

#ifdef DEBUG
    #define DEBUG_CMD(cmd) {puts("+ DEBUG"); cmd; puts("- DEBUG");}
#else
    #define DEBUG_CMD(cmd) {}
#endif

const char *progname = "sh";

static char *callname;

/* we use this enum in the global config so we can know
   if a particular option has been initialized */
typedef enum {Ctrue = 1, Cfalse = 0, Cunset = -1} Cbool;

typedef struct {
    Cbool allexport;   // -a or -o allexport
    Cbool errexit;     // -e or -o errexit
    Cbool ignoreeof;   //       -o ignoreeof
    Cbool monitor;     // -m or -o monitor
    Cbool noclobber;   // -C or -o noclobber
    Cbool noglob;      // -f or -o noglob
    Cbool remember;    // -h
    Cbool noexec;      // -n or -o noexec
    Cbool nolog;       //       -o nolog
    Cbool notify;      // -b or -o notify
    Cbool nounset;     // -u or -o nounset
    Cbool verbose;     // -v or -o verbose
    Cbool vi;          //       -o vi
    Cbool xtrace;      // -x or -o xtrace
    Cbool cmdstr;      // -c
    Cbool cmdstdin;    // -s;
    Cbool interactive; // -i
} Config;         

/* not pretty, but i think it's the less ugly way to do this.
   we could require Cunset to be 0, but then Cfalse wouldnt be
   false anymore... */
static Config gConfig = {Cunset,Cunset,Cunset,Cunset,Cunset,Cunset,
                         Cunset,Cunset,Cunset,Cunset,Cunset,Cunset,
                         Cunset,Cunset,Cunset,Cunset,Cunset};

static void parse_argv(int *argc, char **argv[]);
static void dump_config(void);
static void usage(void);

/* parse the argv, init global config
 * *** this function modifies the argc and argv so that when
 * it returns, argv points to the 1st non-option and 
 * argc is the _actual_ count. We also assume that the
 * caller decremented the argc and pointed argv to its
 * second field before calling */
void parse_argv(int *argc, char **argv[]) 
{
    int j,len;
    char *str;
    Cbool enable;
    bool operands;

    while(*argc != 0) 
    {
        str = *argv[0];
        len = strlen(str);

        /* if not a flag, stop parsing flags */
        if(str[0] != '-' && str[0] != '+')
            break;

        /* if its a single '-', discard and stop parsing */
        if(str[0] == '-' && len == 1) {
            argv++;
            (*argc)--;
            break;
        }

        /* if we get to this point, we can start analyzing
           the string. First, we want to know if we enable 
           or disable. Also, keep in mind that disable opts
           have precedence over enable opts */
        enable = (Cbool) str[0] == '-';

        /* the -o <option> case is a little more complicated,
           so let's treat it first */
        if(str[1] == 'o') {
            /* we are unforgiving on bad input format */
            if(len != 2) {
                fprintf(stderr,"ERROR: unrecognized flag: %s\n", str);
                usage();
                exit(1);
            }

            /* we need an option string */
            if(*argc == 1) {
                fprintf(stderr,"ERROR: option string needed for %s\n", str);
                exit(1);
            }
            
            /* now we are sure we have one, let's jump to it */
            (*argc)--;
            str = *(++(*argv));

            /* let's try to identify it. */
            if(strcmp(str, "allexport")        == 0 && gConfig.allexport) {
                gConfig.allexport = enable;
            } else if(strcmp(str, "errexit")   == 0 && gConfig.errexit)   {
                gConfig.errexit   = enable;
            } else if(strcmp(str, "ignoreeof") == 0 && gConfig.ignoreeof) {
                gConfig.ignoreeof = enable;
            } else if(strcmp(str, "monitor")   == 0 && gConfig.monitor)   {
                gConfig.monitor   = enable;
            } else if(strcmp(str, "noclobber") == 0 && gConfig.noclobber) {
                gConfig.noclobber = enable;
            } else if(strcmp(str, "noglob")    == 0 && gConfig.noglob)    {
                gConfig.noglob    = enable;
            } else if(strcmp(str, "noexec")    == 0 && gConfig.noexec)    {
                gConfig.noexec    = enable;
            } else if(strcmp(str, "nolog")     == 0  && gConfig.nolog)    {
                gConfig.nolog     = enable;
            } else if(strcmp(str, "notify")    == 0 && gConfig.notify)    {
                gConfig.notify    = enable;
            } else if(strcmp(str, "nounset")   == 0 && gConfig.nounset)   {
                gConfig.nounset   = enable;
            } else if(strcmp(str, "verbose")   == 0 && gConfig.verbose)   {
                gConfig.verbose   = enable;
            } else if(strcmp(str, "vi")        == 0 && gConfig.vi)        {
                gConfig.vi        = enable;
            } else if(strcmp(str, "xtrace")    == 0 && gConfig.xtrace)    {
                gConfig.xtrace    = enable;
            } else {
                /* ohoh, bad input! */
                fprintf(stderr, "ERROR: invalid option string: %s\n", str);
                exit(1);
            }
            /* success! continue with next string */
            (*argc)--;
            (*argv)++;
            continue;
        }
            
        /* if we get here, we are looking for single character options.
           we loop over all characters in the string. */
        for(j = 1; j < len; j++)
        {
            /* try to identify char at position */
            switch(str[j]) {
            case 'a':
                if(gConfig.allexport) gConfig.allexport = enable;
                break;
            case 'e':
                if(gConfig.errexit)   gConfig.errexit   = enable;
                break;
            case 'm':
                if(gConfig.monitor)   gConfig.monitor   = enable;
                break;
            case 'C':
                if(gConfig.noclobber) gConfig.noclobber = enable;
                break;
            case 'f':
                if(gConfig.noglob)    gConfig.noglob    = enable;
                break;
            case 'h':
                if(gConfig.remember)  gConfig.remember  = enable;
                break;
            case 'n':
                if(gConfig.noexec)    gConfig.noexec    = enable;
                break;
            case 'b':
                if(gConfig.notify)    gConfig.notify    = enable;
                break;
            case 'u':
                if(gConfig.nounset)   gConfig.nounset   = enable;
                break;
            case 'v':
                if(gConfig.verbose)   gConfig.verbose   = enable;
                break;
            case 'x':
                if(gConfig.xtrace)    gConfig.xtrace    = enable;
                break;
            case 'c':
                gConfig.cmdstr      = Ctrue;
                break;
            case 's':
                gConfig.cmdstdin    = Ctrue;
                break;
            case 'i':
                gConfig.interactive = Ctrue;
                break;
            default:
                /* oops, bad input! */
                fprintf(stderr,"ERROR: unrecognized flag: %c\n", str[j]);
                usage();
                exit(1);
            }
        }
        
        /* we successfuly parsed the whole string! lets
           decrement the argc, point the argv to the next 
           string and try again. */
        (*argc)--;
        (*argv)++;
    }

    /* lets make some adjustment. */
    
    operands = *argc > 0;

    /* -c has precedence over -s */ // FIXME not sure if valid
    if(gConfig.cmdstr == Ctrue)
        gConfig.cmdstdin = Cfalse;

    /* if -c not specified and there are no operands, -s is assumed */
    if(!operands && gConfig.cmdstr != Ctrue) 
        gConfig.cmdstdin = Ctrue;

    /* if there are no operands and stdin and stderr are connected to
       a terminal, -i is assumed */
    if(!operands && isatty(fileno(stdin)) && isatty(fileno(stderr)))
        gConfig.interactive = Ctrue;

    /* if -i, -m is enabled by default */
    if(gConfig.interactive == Ctrue && gConfig.monitor)
        gConfig.monitor = Ctrue;

    /* at this point, we should be good to go. We only need to 
       set anything that is still Cunset to Cfalse */
    if(gConfig.allexport   == Cunset) gConfig.allexport   = Cfalse;
    if(gConfig.errexit     == Cunset) gConfig.errexit     = Cfalse;
    if(gConfig.ignoreeof   == Cunset) gConfig.ignoreeof   = Cfalse;
    if(gConfig.monitor     == Cunset) gConfig.monitor     = Cfalse;
    if(gConfig.noclobber   == Cunset) gConfig.noclobber   = Cfalse;
    if(gConfig.noglob      == Cunset) gConfig.noglob      = Cfalse;
    if(gConfig.remember    == Cunset) gConfig.remember    = Cfalse;
    if(gConfig.noexec      == Cunset) gConfig.noexec      = Cfalse;
    if(gConfig.nolog       == Cunset) gConfig.nolog       = Cfalse;
    if(gConfig.notify      == Cunset) gConfig.notify      = Cfalse;
    if(gConfig.nounset     == Cunset) gConfig.nounset     = Cfalse;
    if(gConfig.verbose     == Cunset) gConfig.verbose     = Cfalse;
    if(gConfig.vi          == Cunset) gConfig.vi          = Cfalse;
    if(gConfig.xtrace      == Cunset) gConfig.xtrace      = Cfalse;
    if(gConfig.cmdstr      == Cunset) gConfig.cmdstr      = Cfalse;
    if(gConfig.cmdstdin    == Cunset) gConfig.cmdstdin    = Cfalse;
    if(gConfig.interactive == Cunset) gConfig.interactive = Cfalse;
    
    /* all done :) */
}

void dump_config(void)
{
    if(gConfig.allexport == Cunset) 
        printf("allexport   : unset\n");
    else if(gConfig.allexport) 
        printf("allexport   : true\n");
    else
        printf("allexport   : false\n");

    if(gConfig.errexit == Cunset) 
        printf("errexit     : unset\n");
    else if(gConfig.errexit) 
        printf("errexit     : true\n");
    else
        printf("errexit     : false\n");

    if(gConfig.ignoreeof == Cunset) 
        printf("ignoreeof   : unset\n");
    else if(gConfig.ignoreeof) 
        printf("ignoreeof   : true\n");
    else
        printf("ignoreeof   : false\n");

    if(gConfig.monitor == Cunset) 
        printf("monitor     : unset\n");
    else if(gConfig.monitor) 
        printf("monitor     : true\n");
    else
        printf("monitor     : false\n");

    if(gConfig.noclobber == Cunset) 
        printf("noclobber   : unset\n");
    else if(gConfig.noclobber) 
        printf("noclobber   : true\n");
    else
        printf("noclobber   : false\n");

    if(gConfig.noglob == Cunset) 
        printf("noglob      : unset\n");
    else if(gConfig.noglob) 
        printf("noglob      : true\n");
    else
        printf("noglob      : false\n");

    if(gConfig.remember == Cunset) 
        printf("remember    : unset\n");
    else if(gConfig.remember) 
        printf("remember    : true\n");
    else
        printf("remember    : false\n");

    if(gConfig.noexec == Cunset) 
        printf("noexec      : unset\n");
    else if(gConfig.noexec) 
        printf("noexec      : true\n");
    else
        printf("noexec      : false\n");

    if(gConfig.nolog == Cunset) 
        printf("nolog       : unset\n");
    else if(gConfig.nolog) 
        printf("nolog       : true\n");
    else
        printf("nolog       : false\n");

    if(gConfig.notify == Cunset) 
        printf("notify      : unset\n");
    else if(gConfig.notify) 
        printf("notify      : true\n");
    else
        printf("notify      : false\n");

    if(gConfig.nounset == Cunset) 
        printf("nounset     : unset\n");
    else if(gConfig.nounset) 
        printf("nounset     : true\n");
    else
        printf("nounset     : false\n");

    if(gConfig.verbose == Cunset) 
        printf("verbose     : unset\n");
    else if(gConfig.verbose) 
        printf("verbose     : true\n");
    else
        printf("verbose     : false\n");

    if(gConfig.vi == Cunset) 
        printf("vi          : unset\n");
    else if(gConfig.vi) 
        printf("vi          : true\n");
    else
        printf("vi          : false\n");

    if(gConfig.xtrace == Cunset) 
        printf("xtrace      : unset\n");
    else if(gConfig.xtrace) 
        printf("xtrace      : true\n");
    else
        printf("xtrace      : false\n");

    if(gConfig.cmdstr == Cunset) 
        printf("cmdstr      : unset\n");
    else if(gConfig.cmdstr) 
        printf("cmdstr      : true\n");
    else
        printf("cmdstr      : false\n");

    if(gConfig.cmdstdin == Cunset) 
        printf("cmdstdin    : unset\n");
    else if(gConfig.cmdstdin) 
        printf("cmdstdin    : true\n");
    else
        printf("cmdstdin    : false\n");

    if(gConfig.interactive == Cunset) 
        printf("interactive : unset\n");
    else if(gConfig.interactive) 
        printf("interactive : true\n");
    else
        printf("interactive : false\n");
}

/* print usage text */
void usage(void)
{
    printf("usage: %s [-/+ abCefhimnuvz][-/+ o <option>] [<non-options>]\n"
           "where <non-options> can be:\n"
           "    <command file> [<argument>...]\n"
           "    -c <command string> [<command name> [argument...]]\n"
           "    -s [argument]\n", progname);
} 

int main(int argc, char *argv[])
{
    /* Welcome to main! */ 

    /* our first task is parsing the argv.
       the function parse_argv() requires us 
       to decrement argc and point argv to 
        its second field, so lets do that */
    callname = argv[0];
    argc--;
    argv++;
    parse_argv(&argc, &argv);

    /* if reading from cmd string, fail if there are no operands */
    if(gConfig.cmdstr == Ctrue) {
        if( argc == 0) {
            fprintf(stderr, "ERROR: -c: missing command string\n");
            exit(1);
        } else {
            input_set_cmdstr(argv[0]);
        }
    }

    DEBUG_CMD(dump_config());

    return 0;
}

