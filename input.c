#include <stdbool.h>
#include <string.h>

#include "input.h"

#define CMDSTR      0
#define INTERACTIVE 1

static int input_type;

static char *cmdstr;
static int   cmdstrlen = 0;


void input_set_cmdstr(char *str)
{
    input_type = CMDSTR;
    cmdstr     = str;   
    cmdstrlen  = strlen(cmdstr); 
}
   
int input_get_next_char(void)
{
    int c;

    if(cmdstrlen == 0) {
        c = -1;
    } else {
        c = *(cmdstr++);
        cmdstrlen--;
    }
    return c;
}
         
