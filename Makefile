TARGET = sh
CC = gcc
CFLAGS = -O2 -Wall -Wextra -Werror -std=c99 -pedantic -pedantic-errors
CFLAGS += -Wno-unused-function -Wno-unused-but-set-variable

OBJS = $(patsubst %.c,%.o,$(wildcard *.c)) 

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

debug: CFLAGS += -DDEBUG
debug: $(TARGET)

clean:
	rm -f *.o
	rm -f $(TARGET)
