#ifndef __INPUT_H__
#define __INPUT_H__

void input_set_cmdstr(char *str);
int  input_get_next_char(void); 

#endif
